## Versions
![AIE](https://img.shields.io/badge/AIE--Version-v1.5.1-green.svg)
![MC](https://img.shields.io/badge/MC--Version-v1.7.10-green.svg)
![Java](https://img.shields.io/badge/Java--Version-v1.8-blue.svg)


## Info
This is a fork to fix this Health boost bug. This isn't perfect in any way. 
If you find any bug you can directly report them to me or on the gitlab but I probably won't fix them right away.

Read this if you want to know what AIE is.
https://dev.bukkit.org/projects/advanced-item-effects

The download can be found on gitlab, chose the version which you want and download the deploy artifact.
https://gitlab.com/DwarfyAssassin/aie/tags

### New features
- New command /aietrigger
  - It removes and reapplies all effects.
  - Permission: AIE.trigger
- Disabled effects
  - Instant health
  - Instant damage
  - Absorpsion