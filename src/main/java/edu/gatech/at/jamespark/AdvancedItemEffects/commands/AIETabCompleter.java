package edu.gatech.at.jamespark.AdvancedItemEffects.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import edu.gatech.at.jamespark.AdvancedItemEffects.Effects;
import edu.gatech.at.jamespark.AdvancedItemEffects.constructors.EffectEffectsList;
import edu.gatech.at.jamespark.AdvancedItemEffects.constructors.PotionEffectsList;

public class AIETabCompleter implements TabCompleter{
	private Effects effects;
	
	public AIETabCompleter(Effects effects) {
		this.effects = effects;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
		if(sender instanceof Player) {
			//initialize variables
			List<String> argsList = new ArrayList<String>();
			int argNumber = 0;
			Player player = (Player) sender;
			
			//add args to argsList for cmds.
			if(cmd.getName().equalsIgnoreCase("aieadd") && args.length == 1) {
				for(String effect : EffectEffectsList.getInstance()) {
					argsList.add(effect);
				}
				for(String effect : PotionEffectsList.getInstance()) {
					argsList.add(effect);
				}
				for(String effect : effects.getItemEffects(player.getItemInHand())) {
					argsList.remove(effect);
				}
				argNumber = 0;
			}
			else if(cmd.getName().equalsIgnoreCase("aieremove") && args.length == 1) {
				argsList = effects.getItemEffects(player.getItemInHand());
				argNumber = 0;
			}
			
			//remove args and return
			if(args[argNumber] != null) {
				argsList.removeIf(s -> !s.contains(args[0].toUpperCase()));
			}
			if(!argsList.isEmpty()) return argsList;
		}
		return null;
	}

}
