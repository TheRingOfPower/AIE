package edu.gatech.at.jamespark.AdvancedItemEffects.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import edu.gatech.at.jamespark.AdvancedItemEffects.Effects;

public class AIETriggerCommand implements CommandExecutor  {
	
	private Effects effects;

    public AIETriggerCommand(Effects effects) {
        this.effects = effects;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    	if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Must execute this command as a player.");
            return false;
        }
    	
        if (!label.equalsIgnoreCase("aietrigger")) return false;
        
        Player player = (Player) sender;
        
        if ((!player.hasPermission("AIE.trigger")) && (!player.isOp())) {
            player.sendMessage(player.getDisplayName() + ChatColor.YELLOW
                    + " lacks \"AIE.trigger\" permission.");
            return false;
        }
        
        effects.removeAllBoundEffects(player);
        effects.addItemEffects(player, true, true, true);
    	
        return true;
    }

}
