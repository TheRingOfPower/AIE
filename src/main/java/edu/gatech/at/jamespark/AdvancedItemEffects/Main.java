package edu.gatech.at.jamespark.AdvancedItemEffects;

import edu.gatech.at.jamespark.AdvancedItemEffects.commands.*;
import edu.gatech.at.jamespark.AdvancedItemEffects.listeners.PlayerEventListener;
import edu.gatech.at.jamespark.AdvancedItemEffects.schedulers.ParticleScheduler;

import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

// https://github.com/jp94/Bukkit_Advanced_Item_Effects/commits/master

// @TODO On item change, show text along with effects
public class Main extends JavaPlugin {
    private ParticleScheduler particleScheduler;
    private Effects effects;

    @Override
    public void onEnable() {
        effects = new Effects(this);
        
        getServer().getPluginManager().registerEvents(new PlayerEventListener(effects, this), this);
        registerCommands();
        registerTabCompleters();
        
        particleScheduler = new ParticleScheduler(effects);
        particleScheduler.runTaskTimer(this, 0, 60);
        this.saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        particleScheduler.cancel();
    }
    
    private void registerCommands() {
    	getCommand("aieadd").setExecutor(new AIEAddCommand(effects));
        getCommand("aieremove").setExecutor(new AIERemoveCommand(effects));
        getCommand("aieclear").setExecutor(new AIEClearCommand(effects));
        getCommand("aietrigger").setExecutor(new AIETriggerCommand(effects));
    }
    
    private void registerTabCompleters() {
    	TabCompleter completer = new AIETabCompleter(effects);
    	getCommand("aieadd").setTabCompleter(completer);
        getCommand("aieremove").setTabCompleter(completer);
    }
}